# MultiEmu### this version is only for Linux, but you can compile for other operating systems

## This is not an emulator, it is just a lightweight front for ordering your games. Based on electronJS - Vuejs - Vuertify


## Project setup
```
npm i
```

### Compiles and hot-reloads for development
```
npm run electron:serve
```

### Compiles and minifies for production
```
npm run electron:build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
