import Vue from 'vue'
import App from './App.vue'
import rutas from './router';
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.config.productionTip = false

new Vue({
  vuetify,
  router: rutas,
  render: h => h(App)
}).$mount('#app')
