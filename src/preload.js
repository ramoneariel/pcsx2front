const { contextBridge } = require('electron');
const userData = require('electron').remote.app.getPath('userData'); // generaly ~user/.config/multiemu/
const process = require('child_process').exec;
const fs = require('fs');
const request = require('request');
const path = require('path');
/**
 *  Using nedb for storing local data
 *  @author ramoneariel
 *  
 *
 */

let Datastore = require('nedb')
  , folders = new Datastore({ filename: path.join(userData, 'database/folders.db'), autoload: true })
  , games = new Datastore({ filename: path.join(userData, 'database/games.db'), autoload: true })
  , platforms = new Datastore({ filename: path.join(userData, 'database/platforms.db'), autoload: true });


/**
 * Init database on first search
 * @author ramoneariel
 */
async function initialice() {
    // create folder for store images
    if(!fs.existsSync(path.join(userData, 'images')))
      fs.mkdirSync(path.join(userData, 'images'), 0744);

    let platform = [{
        id: 1,
        name: "PlayStation 2", 
        options: "--nogui --fullscreen" , 
        id_api: 11, 
        filter: ["iso","bin","nrg","mdf"],
        image: "ps2.png",
        install: [{distro:"ubuntu/debian", exec:"sudo apt install pcsx2"}],
        installedPath: "/usr/games/PCSX2"
      },{
        id: 2,
        name: "Wii",
        options: "",
        id_api: 13,
        filters: ["iso", "wbf"],
        image: "wii.png",
        install: [{distro:"ubuntu/debian", exec: "sudo apt install dolphin-emu"}],
        installedPath: "/usr/games/dolphin-emu"
      }];

    return new Promise((resolve, reject) => {
        platforms.insert(platform, function(error, results) {
            if(error) console.log(error);

            return resolve(results);
        })
    })
}

/**
 * validChanel are events names to call from front end
 * @author ramoneariel
 */
const validChannels = ['UPDATE_PLATFORM', 'PLATFORMS', 'ADD_GAME', 'GAMES', 'GAME_NAMES', 'ADD_FOLDER', 'RUN_GAME', 'GAME_DATA', 'REMOVE_FOLDERS', 'FOLDERS', 'SAVE_IMAGE', 'IMAGE_PATH'];
contextBridge.exposeInMainWorld(
  'secure', {
    // run shell commands
    execute: (channel, data) => {
      return new Promise((resolve, reject) => {
        if (validChannels.includes(channel)) {

          process(data, function (error, stdout, stderr) {
            if(error)
              return reject(error);
            
            return resolve(stdout);
          });
      }
    })
     
    },
    // exchange data from database to front end and vice versa
    exchange: (channel, data) => {
      return new Promise((resolve, reject) => {
        if (!validChannels.includes(channel))
          return reject("NO AVAILABLE CHANEL");
        
        switch (channel) {
          case 'UPDATE_PLATFORM':
            return resolve(updatePlatform(data));
          case 'PLATFORMS':
            return resolve(getAllPlatforms());
          case 'ADD_FOLDER':
            return resolve(addFolder(data));
          case 'FOLDERS':
            return resolve(getAllFolders());
          case 'GAMES':
            return resolve(getAllGames());
          case 'GAME_DATA':
            return resolve(getGameData(data));
          case 'ADD_GAME':
            return resolve(saveGame(data));
          case 'SAVE_IMAGE':
            return resolve(saveImage(data));
          case 'GAME_NAMES':
            return resolve(getGameNames());
          case 'IMAGE_PATH':
            return resolve(getIMagePath());
        }
      })
    },
  },
);

/**
 * @author ramoneariel
 * @returns array of games
 */
async function getAllGames() {
    return new Promise((resolve, reject) => {
        games.find({}).exec(function(error, results) {
            if(error) return reject(error);

            return resolve(results || []);
        })
    });
}

/**
 * Save a new game, data is defined on front end
 * @param {*} data 
 * @returns database response
 */
async function saveGame(data) {
    return new Promise((resolve, reject) => {
        games.insert(data, function(error, result) {
            if(error) return reject(error);

            return resolve(result);
        });
    })
}
/**
 * 
 * @returns 
 */
async function getGameNames() {
    return new Promise((resolve, reject) => {
        games.find({}, function(error, results) {
            if(error) return reject(error);

            return resolve(results);
        })      
    })
}

async function addFolder(data) {
    return new Promise((resolve, reject) => {
        folders.insert(data, function(error, results) {
            if(error) return reject(error);

            return resolve(results);
        })
    })
}

async function getAllFolders() {
    return new Promise((resolve, reject) => {
        folders.find({}, function(error, results) {
            if(error) return reject(error);

            return resolve(results || []);
        })
    })
}


async function getAllPlatforms() {
    return new Promise((resolve, reject) => {
        platforms.find({}, function(error, results) {
            if(error) return reject(error);

            if(results.length == 0) {
                initialice();
                getAllPlatforms();
            }
            return resolve(results);
        })    
    })
}

async function updatePlatform(data) {
  return new Promise((resolve, reject) => {
    platforms.update({id: data.id}, {$set: {path: data.path}}, {}, function(error, results) {
      if(error) return reject(error);

      return resolve(results);
    })
  })
}

/**
 * Save an image on local folder
 * @param {url, fileName} data 
 * @returns boolean
 */
async function saveImage(data) {
  return new Promise((resolve, reject) => {
      return request(data.url).pipe(fs.createWriteStream(userData + '/images/' + data.fileName)).on('close', function(err, resp, body) {
        if(err) return reject(err);

        return resolve(resp);
      });
  })
}

async function getIMagePath() {
  return new Promise((resolve, reject) => {
    return resolve(userData + '/images/');
  })
}