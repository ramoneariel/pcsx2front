import Vue from "vue";
import VueRouter from 'vue-router';


Vue.use(VueRouter);
const router = new VueRouter({
 // mode: 'history',
  base: "/",
  routes: [
    {
      path: '/',
      redirect: '/games',
      component: () => import('../components/Shell.vue'),
      children: 
        [
          {
            path: '/games',
            name: 'games',
            component: () => import('../pages/Games.vue'),
          },
          {
            path: '/scan',
            name: 'scan',
            component: () => import('../pages/ScanFolders.vue'),
          },
          {
            path: '/config',
            name: 'config',
            component: () => import('../pages/Config.vue'),
          },
          {
            path: '/about',
            name: 'about',
            component: () => import('../pages/About.vue'),
          },

        ]
    }
  ]
});




export default router;