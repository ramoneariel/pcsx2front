'use strict'

const { contextBridge } = require('electron');
const process = require('child_process').exec;
const fs = require('fs');
const os = require('os');

const sqlite3 = require('sqlite3').verbose();
let db;
let fdb = os.tmpdir() + 'database.db';


async function conn () {
  return new Promise((resolve, reject) => {
    if (!db || !db.open) {
      db = new sqlite3.Database(fdb);
      if(!fs.existsSync(fdb)) {
        fs.openSync(fdb, "w");
        let tFolders = 'CREATE TABLE "folders" ("id" INTEGER PRIMARY KEY AUTOINCREMENT, "name" TEXT, "path" BLOB, "platform" INTEGER, "autoscan" INTEGER)';
        let tGames = 'CREATE TABLE "games" ("id" INTEGER PRIMARY KEY AUTOINCREMENT, "name" TEXT, "folder" TEXT, "file" TEXT, "path" TEXT, "platform" INTEGER, "manufacturer" INTEGER, "overview" TEXT, "genere"	TEXT, "date" BLOB, "images"	BLOB, "raiting"	NUMERIC, "votes" INTEGER, "played" INTEGER, "id_gamedb"	INTEGER)';
        let tPlatforms = 'CREATE TABLE "platforms" ("id" INTEGER PRIMARY KEY AUTOINCREMENT, "name" TEXT, "path" BLOB, "options"	TEXT, "id_api" INTEGER, "filter" TEXT, "install" BLOB)';
        let index = 'CREATE UNIQUE INDEX index_games ON games(file)';
        let iPlatforms = "INSERT INTO platforms (name, options, id_api, filter) VALUES ('PlayStation 2', '--nogui', '11', '[iso,bin,nrg,mdf]')";
        db.serialize(() => {
          db.run(tFolders).run(tGames).run(tPlatforms).run(index).run(iPlatforms);
        })
      }
      return resolve(db);
    }else
      return resolve(db)
  })
}


const validChannels = ['UPDATE_PLATFORM', 'PLATFORMS', 'ADD_GAME', 'GAMES', 'GAME_NAMES', 'ADD_FOLDER', 'RUN_GAME', 'GAME_DATA', 'REMOVE_FOLDERS', 'FOLDERS', 'SAVE_IMAGE'];
contextBridge.exposeInMainWorld(
  'secure', {
    execute: (channel, data) => {
      return new Promise((resolve, reject) => {
        if (validChannels.includes(channel)) {

          process(data, function (error, stdout, stderr) {
            if(error)
              return reject(error);
            
            return resolve(stdout);
          });
      }
    })
     
    },
    exchange: (channel, data) => {
      return new Promise((resolve, reject) => {
        if (!validChannels.includes(channel))
          return reject("NO AVAILABLE CHANEL");
        
        switch (channel) {
          case 'UPDATE_PLATFORM':
            return resolve(updatePlatform(data));
          case 'PLATFORMS':
            return resolve(getAllPlatforms());
          case 'ADD_FOLDER':
            return resolve(addFolder(data));
          case 'FOLDERS':
            return resolve(getAllFolders());
          case 'GAMES':
            return resolve(getAllGames());
          case 'GAME_DATA':
            return resolve(getGameData(data));
          case 'ADD_GAME':
            return resolve(saveGame(data));
          case 'SAVE_IMAGE':
            return resolve(saveImage(data));
          case 'GAME_NAMES':
            return resolve(getGameNames());
        }
      })
    },
  },
);

async function saveGame(data) {
  let db = await conn();
  return new Promise((resolve, reject) => {
    let prepare = db.prepare('INSERT INTO games (name, folder, file, path, platform, manufacturer, overview, genere, date, images, raiting, votes, id_gamedb, played) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)')
    prepare.run(data.name, data.folder, data.file, data.path, data.platform, null, data.overview, data.genere, data.date, data.images, null, null, data.id_gamedb, data.played )
    return prepare.finalize(err => {
      if (err) reject(err)
      resolve()
    })
  })
}

async function updatePlatform(data) {
  let db = await conn();
  return new Promise((resolve, reject) => {
    let prepare = db.prepare('UPDATE platforms SET path = ? WHERE id = ?')
    prepare.run(data.path, data.id)
    return prepare.finalize(err => {
      if (err) reject(err)
      resolve()
    })
  })
}

async function getAllPlatforms() {
  let db = await conn();
  return new Promise((resolve, reject) => {
    return db.all('SELECT id, name, path, options, id_api, filter FROM platforms', (err, rows) => {
      if (err) return reject(err)
      return resolve(rows || [])
    })
  })
}

async function getAllFolders() {
  let db = await conn();
  return new Promise((resolve, reject) => {
    return db.all('SELECT id, name, path, platform, autoscan FROM folders', (err, rows) => {
      if (err) return reject(err)
      return resolve(rows || [])
    })

  })
}

async function addFolder(data) {
  let db = conn();
  return new Promise((resolve, reject) => {
    let prepare = db.prepare('INSERT INTO folders (name, path, platform, autoscan) values (?, ?, ?, ?)')
    prepare.run(data.name, data.path, data.platform, data.autoscan)
    return prepare.finalize(err => {
      if (err) reject(err)
      resolve()
    })
  })
}

async function getAllGames() {
  let db = await conn();
  return new Promise((resolve, reject) => {
    return db.all('SELECT id, name, folder, file, path, platform, manufacturer, overview, genere, date, images, raiting, votes, played FROM games', (err, rows) => {
      if (err) return reject(err)
      return resolve(rows || [])
    })
  })
}

async function setPlayed(data) {
  let db = await conn();
  return new Promise((resolve, reject) => {
    let prepare = db.prepare('UPDATE games SET played = ? WHERE id = ?')
    prepare.run(data.played, data.id)
    return prepare.finalize(err => {
      if (err) reject(err)
      resolve()
    })
  })
}

// async function saveImage(data) {
  // return new Promise((resolve, reject) => {
    // return request(data.url).pipe(fs.createWriteStream("public/images/"+data.name)).on('close', () => {
    //   console.log("FINISH");
    // });
  // })
// }

async function getGameNames() {
  let db = await conn();
  return new Promise((resolve, reject) => {
    return db.all('SELECT file FROM games', (err, rows) => {
      if (err) return reject(err)
      return resolve(rows || [])
    })
  })
}